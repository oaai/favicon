# About

Takes an image, which is expected to be of one layer and
sized to, e.g., 192x192 pixels, and creates (overwrites) a
favicon.ico, containing the image and popular scaled down
versions of it, at home directory (if GIMP was opened first,
then the image) or the directory of the image (if opening
the image opened GIMP), or specified directory and file.

# Requirements

1. GNU/Linux OS. Other OSes could work too, if they can run GIMP.
1. [GIMP] v2.10. Later versions should work too.

[GIMP]: https://www.gimp.org/

# Install

1. Download the script file: `script-fu-favicon.scm`.
1. Move it to one of the directories at: `File -> Preferences ->
Folders -> Scripts`.
1. Launch GIMP or push its: `Filters -> Script-Fu -> Refresh Scripts`.

# Usage

1. Push: `Filters -> Web -> Favicon`.
1. Enter where to save the `favicon.ico` and push `OK`.

# Tips

The number of colors should be kept to a minimum in order to save file
size. Ideally, the image contains three colors or less and an alpha
channel.

The initial image does not have to be 192x192 pixels, but should be at
least this or bigger; and be the square. If it is different than
192x192 pixels, it will be included in the final `favicon.ico`.

Undo will undo all the `favicon.ico` creation actions in GIMP.

If preset sizes to scale down are wanted to be altered, modify the
`script-fu-favicon.scm` file's number(s) in the line:
`(sizes '(180 167 152 128 64))`.
One number there is the X and Y of one image in `favicon.ico`. If the
file `script-fu-favicon.scm` is modified, either the GIMP needs to be
restarted or its scripts refreshed (as described in the last step of
the **Install**).

Symmetric image may be scaled down asymmetrically for approximately
48x48 pixels or less, since no interpolation is done in order to not
introduce the colors of shades, which may increase the size of the
`favicon.ico` significantly. A browser could get a set of images from
a `favicon.ico`, pick the closest one and scale further if it needs
that, possibly with introducing slight shading of its own.

# Copyright

Copyright © 2022 Donatas Klimašauskas

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

Full license is at the COPYING file.
