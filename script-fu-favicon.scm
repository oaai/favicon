(script-fu-register
 "script-fu-favicon"
 "Favicon"
 "Takes an image, which is expected to be of one layer and
sized to, e.g., 192x192 pixels, and creates (overwrites) a
favicon.ico, containing the image and popular scaled down
versions of it, at home directory (if GIMP was opened first,
then the image) or the directory of the image (if opening
the image opened GIMP), or specified directory and file."
 "Donatas Klimašauskas"
 "Donatas Klimašauskas, GPLv3+"
 "2022"
 ""
 SF-IMAGE "Image to Favicon" -1
 SF-DRAWABLE "The one layer of the image" -1
 SF-STRING "Favicon pathname" "favicon.ico"
 )
(script-fu-menu-register "script-fu-favicon" "<Image>/Filters/Web")
(define (script-fu-favicon image drawable filename)
  (let*
      (
       (sizes '(180 167 152 128 64))
       (size)
       (layer)
       (parent 0)
       (position -1)
       (image-origin FALSE)
       )
    (gimp-context-set-interpolation INTERPOLATION-NONE) ; no scaling shades
    (gimp-image-undo-group-start image)
    (while (> (length sizes) 0)
	   (set! size (car sizes))
	   (set! layer (car (gimp-layer-new-from-drawable drawable image)))
	   (gimp-image-insert-layer image layer parent position)
	   (gimp-layer-scale layer size size image-origin)
	   (set! sizes (cdr sizes))
	   )
    (gimp-image-undo-group-end image)
    )
  (gimp-displays-flush)
  (file-ico-save RUN-NONINTERACTIVE image drawable filename filename)
  )
